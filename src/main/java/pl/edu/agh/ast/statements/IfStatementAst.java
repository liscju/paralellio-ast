package pl.edu.agh.ast.statements;

import pl.edu.agh.ast.api.CompoundStatementAst;
import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

import java.util.List;
import java.util.Set;

/**
 * If statement.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 *    expression
 *    ||||||||||||||
 * if ( i < 123456 )
 * {             <----------
 *      j = 15;  <---------- action
 *               <----------
 * }
 * else
 * {             <----------
 *     j = 30;   <---------- else action
 * }             <----------
 * }
 * </pre>
 */
public class IfStatementAst extends CompoundStatementAst{

    private DetailAst expression;
    private DetailAst action;
    private DetailAst elseAction;

    public IfStatementAst(DetailAst expression, DetailAst action) {
        super(DetailAstUtils.createNonNullsList(expression, action));
        this.expression = expression;
        this.action = action;
        this.elseAction = null;
    }

    public IfStatementAst(DetailAst expression, DetailAst action, DetailAst elseAction) {
        super(DetailAstUtils.createNonNullsList(expression, action, elseAction));
        this.expression = expression;
        this.action = action;
        this.elseAction = elseAction;
    }

    public IfStatementAst(DetailAst expression, DetailAst action, DetailAst elseAction, int startIndex, int endIndex) {
        super(DetailAstUtils.createNonNullsList(expression, action, elseAction), startIndex, endIndex);
        this.expression = expression;
        this.action = action;
        this.elseAction = elseAction;
    }

    /**
     * Get expression of an if statement
     * @return expression
     */
    public DetailAst getExpression() {
        return expression;
    }

    /**
     * Get action of an if statement
     * @return action
     */
    public DetailAst getAction() {
        return action;
    }

    /**
     * Get else action of an if statement
     * @return else action
     */
    public DetailAst getElseAction() {
        return elseAction;
    }

    @Override
    public List<DetailAst> getCompoundStatementBody() {
        return DetailAstUtils.createNonNullsList(action, elseAction);
    }

    @Override
    public boolean isControlStmt() {
        return true;
    }

    @Override
    public Set<String> getInputDependencies() {
        return getExpression().getInputDependencies();
    }

    @Override
    public Set<String> getOutputDependencies() {
        return getExpression().getOutputDependencies();
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == expression) {
            expression = targetChildren;
        } else if (childrenToSwap == action) {
            action = targetChildren;
        } else if (childrenToSwap == elseAction) {
            elseAction = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        DetailAst clonedElseAction = null;
        if (elseAction != null) {
            clonedElseAction = elseAction.clone();
        }
        return new IfStatementAst(expression.clone(), action.clone(), clonedElseAction);
    }

}
