package pl.edu.agh.ast.statements;

import pl.edu.agh.ast.api.CompoundStatementAst;
import pl.edu.agh.ast.api.DetailAst;

import java.util.LinkedList;
import java.util.List;

/**
 * List of statements.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * {            <---------
 *     int i;   <--------- statements
 *     j = 15;  <---------
 * }            <---------
 * }
 * </pre>
 */
public class StatementListAst extends CompoundStatementAst {
    public StatementListAst(List<DetailAst> childrens) {
        super(childrens);
    }

    public StatementListAst(List<DetailAst> childrens, int origStartOffset, int origEndOffset) {
        super(childrens, origStartOffset, origEndOffset);
    }

    /**
     * Get statements
     * @return statements
     */
    public List<DetailAst> getStatements() {
        return getChildrens();
    }

    @Override
    public boolean isControlStmt() {
        return true;
    }

    /**
     * Insert given node at the end of child list
     * @param detailAst node to insert
     */
    public void insertNode(DetailAst detailAst) {
        childrens.add(detailAst);
    }

    /**
     * Insert node toInsert in child list after insertPoint
     * @param insertPoint after this child ast is inserted
     * @param toInsert ast to insert
     */
    public void insertAfterNode(DetailAst insertPoint, DetailAst toInsert) {
        childrens.add(childrens.indexOf(insertPoint) + 1, toInsert);
    }

    /**
     * Remove given node from child list
     * @param detailAst node to remove
     */
    public void removeNode(DetailAst detailAst) {
        childrens.remove(detailAst);
    }

    @Override
    public DetailAst clone() {
        List<DetailAst> clonedChildrens = new LinkedList<>();
        for (DetailAst children : childrens) {
            clonedChildrens.add(children.clone());
        }
        return new StatementListAst(clonedChildrens);
    }

}
