package pl.edu.agh.ast.statements;

import pl.edu.agh.ast.api.CompoundStatementAst;
import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Default statement.
 *
 * <p>
 * Used in switch statement to establish default action
 * </p>
 *
 * <p>
 * For now it contains only text inside, we don't parse
 * variable names, initialization values etc
 * </p>
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * default:
 *      i = i + 10;
 *      ^^^^^^^^^^^
 *      |||||||||||
 *      action
 * }
 * </pre>
 */
public class DefaultStatementAst extends CompoundStatementAst {
    private DetailAst action;

    public DefaultStatementAst(DetailAst action) {
        super(DetailAstUtils.createNonNullsList(action));
        this.action = action;
    }

    public DefaultStatementAst(DetailAst action, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(action), origStartOffset, origEndOffset);
        this.action = action;
    }

    /**
     * Get action of default statement.
     * @return action expression.
     */
    public DetailAst getAction() {
        return action;
    }

    @Override
    public boolean isControlStmt() {
        return true;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == action) {
            action = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new DefaultStatementAst(action.clone());
    }

}
