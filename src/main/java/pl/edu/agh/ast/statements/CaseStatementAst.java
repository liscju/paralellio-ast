package pl.edu.agh.ast.statements;

import pl.edu.agh.ast.api.CompoundStatementAst;
import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

import java.util.List;
import java.util.Set;


/**
 * Case statement.
 *
 * <p>
 * It is used in switch to distinguish what action do for what value.
 * </p>
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * expression
 * |||||||
 * case 20:
 *      i++;
 *      ^^^^
 *      ||||
 *      action
 * }
 * </pre>
 */
public class CaseStatementAst extends CompoundStatementAst{
    private DetailAst expression;
    private DetailAst action;

    public CaseStatementAst(DetailAst expression, DetailAst action) {
        super(DetailAstUtils.createNonNullsList(expression, action));
        this.expression = expression;
        this.action = action;
    }

    public CaseStatementAst(DetailAst expression, DetailAst action, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(expression, action), origStartOffset, origEndOffset);
        this.expression = expression;
        this.action = action;
    }

    /**
     * Get expression of case statement.
     * @return expression
     */
    public DetailAst getExpression() {
        return expression;
    }

    /**
     * Get action of case statement.
     * @return action expression
     */
    public DetailAst getAction() {
        return action;
    }

    @Override
    public List<DetailAst> getCompoundStatementBody() {
        return DetailAstUtils.createNonNullsList(action);
    }

    @Override
    public boolean isControlStmt() {
        return true;
    }

    @Override
    public Set<String> getInputDependencies() {
        return getExpression().getInputDependencies();
    }

    @Override
    public Set<String> getOutputDependencies() {
        return getExpression().getOutputDependencies();
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == expression) {
            expression = targetChildren;
        } else if (childrenToSwap == action) {
            action = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new CaseStatementAst(expression.clone(), action.clone());
    }

}
