package pl.edu.agh.ast.statements;

import pl.edu.agh.ast.api.CompoundStatementAst;
import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Labeled statement.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * label
 * ||||||
 * ABECAD:
 *   int a = 20;
 *   ^^^^^^^^^^^
 *   |||||||||||
 *   statement
 * }
 * </pre>
 */
public class LabeledStatementAst extends CompoundStatementAst {
    private String labelName;
    private DetailAst statement;

    public LabeledStatementAst(String labelName, DetailAst statement) {
        super(DetailAstUtils.createNonNullsList(statement));
        this.labelName = labelName;
        this.statement = statement;
    }

    public LabeledStatementAst(String labelName, DetailAst statement, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(statement), origStartOffset, origEndOffset);
        this.labelName = labelName;
        this.statement = statement;
    }

    /**
     * Get label of a statement
     * @return label
     */
    public String getLabel() {
        return labelName;
    }

    /**
     * Get statement
     * @return statement
     */
    public DetailAst getStatement() {
        return statement;
    }

    @Override
    public boolean isControlStmt() {
        return true;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == statement) {
            statement = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new LabeledStatementAst(labelName, statement.clone());
    }

}
