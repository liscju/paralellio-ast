package pl.edu.agh.ast.statements;

import pl.edu.agh.ast.api.CompoundStatementAst;
import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

import java.util.List;
import java.util.Set;

/**
 * Switch statement.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 *        expression
 *        ||||||||||
 * switch (my_value)
 * {         <-----------
 *     ..... <----------- action
 * }         <-----------
 * }
 * </pre>
 */
public class SwitchStatementAst extends CompoundStatementAst {
    private DetailAst expression;
    private DetailAst action;

    public SwitchStatementAst(DetailAst expression, DetailAst action) {
        super(DetailAstUtils.createNonNullsList(expression, action));
        this.expression = expression;
        this.action = action;
    }

    public SwitchStatementAst(DetailAst expression, DetailAst action, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(expression, action), origStartOffset, origEndOffset);
        this.expression = expression;
        this.action = action;
    }

    /**
     * Get expression of switch statement
     * @return expression
     */
    public DetailAst getExpression() {
        return expression;
    }

    /**
     * Get action of a statement
     * @return action
     */
    public DetailAst getAction() {
        return action;
    }

    @Override
    public List<DetailAst> getCompoundStatementBody() {
        return DetailAstUtils.createNonNullsList(action);
    }

    @Override
    public boolean isControlStmt() {
        return true;
    }

    @Override
    public Set<String> getInputDependencies() {
        return getExpression().getInputDependencies();
    }

    @Override
    public Set<String> getOutputDependencies() {
        return getExpression().getOutputDependencies();
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == expression) {
            expression = targetChildren;
        } else if (childrenToSwap == action) {
            action = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new SwitchStatementAst(expression.clone(), action.clone());
    }

}
