package pl.edu.agh.ast.operators.bitwise.unary;

import pl.edu.agh.ast.api.DetailAst;

import java.util.LinkedList;

/**
 * Size of given type
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * sizeof(int);
 * }
 * </pre>
 */
public class SizeOfTypeAst extends DetailAst {
    private String type;

    public SizeOfTypeAst(String type) {
        super(new LinkedList<>());
        this.type = type;
    }

    public SizeOfTypeAst(String type, int origStartOffset, int origEndOffset) {
        super(new LinkedList<>(), origStartOffset, origEndOffset);
        this.type = type;
    }

    /**
     * Type to calculate size
     * @return type
     */
    public String getType() {
        return type;
    }

    @Override
    public DetailAst clone() {
        return new SizeOfTypeAst(type);
    }

}
