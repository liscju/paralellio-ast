package pl.edu.agh.ast.operators.bitwise.references;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Call function with given arguments.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * printf('Hello world');
 * }
 * </pre>
 */
public class CallFunctionExpressionAst extends DetailAst {
    private DetailAst function;
    private FunctionArgsAst functionArgsAst;

    public CallFunctionExpressionAst(DetailAst function, FunctionArgsAst functionArgsAst) {
        super(DetailAstUtils.createNonNullsList(function, functionArgsAst));
        this.function = function;
        this.functionArgsAst = functionArgsAst;
    }

    public CallFunctionExpressionAst(DetailAst function, FunctionArgsAst functionArgsAst, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(function, functionArgsAst),
                origStartOffset, origEndOffset);
        this.function = function;
        this.functionArgsAst = functionArgsAst;
    }

    /**
     * Get function to call
     * @return function to call
     */
    public DetailAst getFunction() {
        return function;
    }

    /**
     * Get arguments of invocation
     * @return arguments
     */
    public FunctionArgsAst getArguments() {
        return functionArgsAst;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == function) {
            function = targetChildren;
        } else if (childrenToSwap == functionArgsAst){
            functionArgsAst = (FunctionArgsAst) targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new CallFunctionExpressionAst(
                function.clone(), (FunctionArgsAst) functionArgsAst.clone());
    }

}
