package pl.edu.agh.ast.operators.bitwise.multiple_args;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Rest of division of two expressions.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * 25 % 5;
 * }
 * </pre>
 */
public class DivRestExpressionsAst extends DetailAst {
    private DetailAst leftSide;
    private DetailAst rightSide;

    public DivRestExpressionsAst(DetailAst leftSide, DetailAst rightSide) {
        super(DetailAstUtils.createNonNullsList(leftSide, rightSide));
        this.leftSide = leftSide;
        this.rightSide = rightSide;
    }

    public DivRestExpressionsAst(DetailAst leftSide, DetailAst rightSide, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(leftSide, rightSide), origStartOffset, origEndOffset);
        this.leftSide = leftSide;
        this.rightSide = rightSide;
    }

    /**
     * Return left side of the expression
     * @return left side of the expression
     */
    public DetailAst getLeftExpression() {
        return leftSide;
    }

    /**
     * Return right side of the expression
     * @return right side of the expression
     */
    public DetailAst getRightExpression() {
        return rightSide;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == leftSide) {
            leftSide = targetChildren;
        } else if (childrenToSwap == rightSide) {
            rightSide = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new DivRestExpressionsAst(leftSide.clone(), rightSide.clone());
    }

}
