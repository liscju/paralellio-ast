package pl.edu.agh.ast.operators.bitwise.assignments;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;
import pl.edu.agh.ast.api.DetailDoAssignmentAst;

/**
 * Multiply assignment expression.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * i *= 10;
 * }
 * </pre>
 */
public class MulAssignmentExpressionAst extends DetailDoAssignmentAst {
    private DetailAst leftSide;
    private DetailAst rightSide;

    public MulAssignmentExpressionAst(DetailAst leftSide, DetailAst rightSide) {
        super(DetailAstUtils.createNonNullsList(leftSide, rightSide));
        this.leftSide = leftSide;
        this.rightSide = rightSide;
    }

    public MulAssignmentExpressionAst(DetailAst leftSide, DetailAst rightSide,
                                      int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(leftSide, rightSide), origStartOffset, origEndOffset);
        this.leftSide = leftSide;
        this.rightSide = rightSide;
    }

    @Override
    public DetailAst getLeftSide() {
        return leftSide;
    }

    @Override
    public DetailAst getRightSide() {
        return rightSide;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == leftSide) {
            leftSide = targetChildren;
        } else if (childrenToSwap == rightSide) {
            rightSide = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new MulAssignmentExpressionAst(leftSide.clone(), rightSide.clone());
    }

}
