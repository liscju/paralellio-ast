package pl.edu.agh.ast.operators.bitwise.references;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Get address of given expression.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * &(my_pointer + 2);
 * }
 * </pre>
 */
public class GetAddressExpressionAst extends DetailAst{
    private DetailAst exprToGetAddress;

    public GetAddressExpressionAst(DetailAst exprToGetAddress) {
        super(DetailAstUtils.createNonNullsList(exprToGetAddress));
        this.exprToGetAddress = exprToGetAddress;
    }

    public GetAddressExpressionAst(DetailAst exprToGetAddress,
                                   int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(exprToGetAddress), origStartOffset, origEndOffset);
        this.exprToGetAddress = exprToGetAddress;
    }

    /**
     * Get expression to calculate address
     * @return expression
     */
    public DetailAst getArgument() {
        return exprToGetAddress;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == exprToGetAddress) {
            exprToGetAddress = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new GetAddressExpressionAst(exprToGetAddress.clone());
    }

}
