package pl.edu.agh.ast.operators.bitwise.unary;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Bitwise not of value.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * ~0x255
 * }
 * </pre>
 */
public class BitwiseNotExpressionAst extends DetailAst{
    private DetailAst argument;

    public BitwiseNotExpressionAst(DetailAst argument) {
        super(DetailAstUtils.createNonNullsList(argument));
        this.argument = argument;
    }

    public BitwiseNotExpressionAst(DetailAst argument, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(argument), origStartOffset, origEndOffset);
        this.argument = argument;
    }

    /**
     * Argument to bitwise not
     * @return argument
     */
    public DetailAst getArgument() {
        return argument;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == argument) {
            argument = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new BitwiseNotExpressionAst(argument.clone());
    }

}
