package pl.edu.agh.ast.operators.bitwise.references;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Get value that pointers 'points' to.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * *(my_pointer);
 * }
 * </pre>
 */
public class GetIndirectionValueExpressionAst extends DetailAst{
    private DetailAst exprToGetIndirection;

    public GetIndirectionValueExpressionAst(DetailAst exprToGetIndirection) {
        super(DetailAstUtils.createNonNullsList(exprToGetIndirection));
        this.exprToGetIndirection = exprToGetIndirection;
    }

    public GetIndirectionValueExpressionAst(DetailAst exprToGetIndirection, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(exprToGetIndirection), origStartOffset, origEndOffset);
        this.exprToGetIndirection = exprToGetIndirection;
    }

    /**
     * Get expression for which we should get indirection value.
     * @return expression
     */
    public DetailAst getArgument() {
        return exprToGetIndirection;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (targetChildren == exprToGetIndirection) {
            exprToGetIndirection = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new GetIndirectionValueExpressionAst(exprToGetIndirection.clone());
    }

}
