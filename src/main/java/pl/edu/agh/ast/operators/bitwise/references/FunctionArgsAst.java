package pl.edu.agh.ast.operators.bitwise.references;

import pl.edu.agh.ast.api.DetailAst;

import java.util.LinkedList;
import java.util.List;

/**
 * Arguments of function invocation.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * printf('Hello', "World')
 *        ^^^^^^^^^^^^^^^^
 *        ||||||||||||||||
 *        This part - list of arguments
 * }
 * </pre>
 */
public class FunctionArgsAst extends DetailAst{
    public FunctionArgsAst(List<DetailAst> childrens) {
        super(childrens);
    }

    public FunctionArgsAst(List<DetailAst> childrens, int origStartOffset, int origEndOffset) {
        super(childrens, origStartOffset, origEndOffset);
    }

    /**
     * Get count of arguments
     * @return count of arguments
     */
    public int getCount() {
        return getChildrenCount();
    }

    @Override
    public DetailAst clone() {
        List<DetailAst> newChildrens = new LinkedList<>();
        for (DetailAst children : childrens) {
            newChildrens.add(children.clone());
        }
        return new FunctionArgsAst(newChildrens);
    }

}
