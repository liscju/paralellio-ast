package pl.edu.agh.ast.operators;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Boolean ternary operator of expressions
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * 20 >= 10 ? true : false;
 * }
 * </pre>
 */
public class TernaryOperatorExpresionAst extends DetailAst{
    private final DetailAst condExpr;
    private final DetailAst trueExpr;
    private final DetailAst falseExpr;

    public TernaryOperatorExpresionAst(DetailAst condExpr, DetailAst trueExpr, DetailAst falseExpr) {
        super(DetailAstUtils.createNonNullsList(condExpr, trueExpr, falseExpr));
        this.condExpr = condExpr;
        this.trueExpr = trueExpr;
        this.falseExpr = falseExpr;
    }

    public TernaryOperatorExpresionAst(
            DetailAst condExpr, DetailAst trueExpr, DetailAst falseExpr,
            int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(condExpr, trueExpr, falseExpr),
                origStartOffset, origEndOffset);
        this.condExpr  = condExpr;
        this.trueExpr  = trueExpr;
        this.falseExpr = falseExpr;
    }

    /**
     * Get conditional expression
     * @return conditional expression
     */
    public DetailAst getConditionalExpression() {
        return condExpr;
    }

    /**
     * Get true expression of operator
     * @return expression
     */
    public DetailAst getTrueExpression() {
        return trueExpr;
    }

    /**
     * Get false expression of operator
     * @return expression
     */
    public DetailAst getFalseExpression() {
        return falseExpr;
    }

    @Override
    public DetailAst clone() {
        return new TernaryOperatorExpresionAst(
                condExpr.clone(), trueExpr.clone(), falseExpr.clone());
    }

}
