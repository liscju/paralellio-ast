package pl.edu.agh.ast.operators.bitwise.unary;

import com.google.common.collect.Sets;
import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

import java.util.Set;

/**
 * Post decrement of given value.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * 15--;
 * }
 * </pre>
 */
public class PostDecrementExpressionAst extends DetailAst{
    private DetailAst argument;

    public PostDecrementExpressionAst(DetailAst argument) {
        super(DetailAstUtils.createNonNullsList(argument));
        this.argument = argument;
    }

    public PostDecrementExpressionAst(DetailAst argument, int origStartOffset, int origStopOffset) {
        super(DetailAstUtils.createNonNullsList(argument), origStartOffset, origStopOffset);
        this.argument = argument;
    }

    /**
     * Value to calculate post decrement
     * @return value
     */
    public DetailAst getArgument() {
        return argument;
    }

    @Override
    public Set<String> getOutputDependencies() {
        return Sets.union(super.getOutputDependencies(),
                          super.getInputDependencies());
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == argument) {
            argument = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new PostDecrementExpressionAst(argument.clone());
    }

}
