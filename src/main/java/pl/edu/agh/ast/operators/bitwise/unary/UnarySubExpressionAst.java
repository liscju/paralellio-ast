package pl.edu.agh.ast.operators.bitwise.unary;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Unary substraction expression.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * -value;
 * }
 * </pre>
 */
public class UnarySubExpressionAst extends DetailAst{
    private DetailAst argument;

    public UnarySubExpressionAst(DetailAst argument) {
        super(DetailAstUtils.createNonNullsList(argument));
        this.argument = argument;
    }

    public UnarySubExpressionAst(DetailAst argument, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(argument), origStartOffset, origEndOffset);
        this.argument = argument;
    }

    /**
     * Expression to calculate unary substraction.
     * @return expression
     */
    public DetailAst getArgument() {
        return argument;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == argument) {
            argument = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new UnarySubExpressionAst(argument.clone());
    }

}
