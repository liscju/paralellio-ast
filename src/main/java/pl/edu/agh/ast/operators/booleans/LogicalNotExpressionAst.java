package pl.edu.agh.ast.operators.booleans;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Logical not of given expression
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * !false;
 * }
 * </pre>
 */
public class LogicalNotExpressionAst extends DetailAst{
    private DetailAst argument;

    public LogicalNotExpressionAst(DetailAst argument) {
        super(DetailAstUtils.createNonNullsList(argument));
        this.argument = argument;
    }

    public LogicalNotExpressionAst(DetailAst argument, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(argument), origStartOffset, origEndOffset);
        this.argument = argument;
    }

    /**
     * Get expression to calculate logical not
     * @return expression
     */
    public DetailAst getArgument() {
        return argument;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == argument) {
            argument = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new LogicalNotExpressionAst(argument.clone());
    }

}
