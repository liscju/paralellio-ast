package pl.edu.agh.ast.operators.bitwise.unary;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Unary add.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * +5;
 * }
 * </pre>
 */
public class UnaryAddExpressionAst extends DetailAst{
    private DetailAst argument;

    public UnaryAddExpressionAst(DetailAst argument) {
        super(DetailAstUtils.createNonNullsList(argument));
        this.argument = argument;
    }

    public UnaryAddExpressionAst(DetailAst argument, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(argument), origStartOffset, origEndOffset);
        this.argument = argument;
    }

    /**
     * Expression to calculate unary add
     * @return expression
     */
    public DetailAst getArgument() {
        return argument;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == argument) {
            argument = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new UnaryAddExpressionAst(argument.clone());
    }

}
