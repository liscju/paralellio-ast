package pl.edu.agh.ast.operators.booleans;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Logical and of two expressions
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * true && true;
 * }
 * </pre>
 */
public class LogicalAndExpresionAst extends DetailAst{
    private DetailAst leftSide;
    private DetailAst rightSide;

    public LogicalAndExpresionAst(DetailAst leftSide, DetailAst rightSide) {
        super(DetailAstUtils.createNonNullsList(leftSide, rightSide));
        this.leftSide = leftSide;
        this.rightSide = rightSide;
    }

    public LogicalAndExpresionAst(DetailAst leftSide, DetailAst rightSide,
                                  int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(leftSide, rightSide), origStartOffset, origEndOffset);
        this.leftSide = leftSide;
        this.rightSide = rightSide;
    }

    /**
     * Left side of the expression
     * @return expression
     */
    public DetailAst getLeftExpression() {
        return leftSide;
    }

    /**
     * Right side of the expression
     * @return expression
     */
    public DetailAst getRightExpression() {
        return rightSide;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == leftSide) {
            leftSide = targetChildren;
        } else if (childrenToSwap == rightSide) {
            rightSide = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new LogicalAndExpresionAst(leftSide.clone(), rightSide.clone());
    }

}
