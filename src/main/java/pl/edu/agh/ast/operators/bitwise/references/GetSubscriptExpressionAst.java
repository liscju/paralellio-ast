package pl.edu.agh.ast.operators.bitwise.references;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;
import pl.edu.agh.ast.others.VariableIdentifierAst;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Get value from table in given index.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * table[2];
 * }
 * </pre>
 */
public class GetSubscriptExpressionAst extends DetailAst{
    private DetailAst table;
    private DetailAst index;

    public GetSubscriptExpressionAst(DetailAst table, DetailAst index) {
        super(DetailAstUtils.createNonNullsList(table, index));
        this.table = table;
        this.index = index;
    }

    public GetSubscriptExpressionAst(DetailAst table, DetailAst index, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(table, index), origStartOffset, origEndOffset);
        this.table = table;
        this.index = index;
    }

    /**
     * Get table
     * @return table
     */
    public DetailAst getTable() {
        return table;
    }

    /**
     * Get index
     * @return index
     */
    public DetailAst getIndex() {
        return index;
    }

    /**
     * Get final table variable name that subscript referses to.
     * @return table variable name, or null if not exists
     */
    public String getFinalTableVariableName() {
        DetailAst iter = this;
        while (iter instanceof GetSubscriptExpressionAst)
            iter = ((GetSubscriptExpressionAst) iter).getTable();
        if (iter instanceof VariableIdentifierAst)
            return ((VariableIdentifierAst) iter).getName();
        else
            return null;
    }

    /**
     * Get list of variables that are used in index of array
     * @return list of variables
     */
    public Set<String> getVariablesUsedInIndex() {
        final Set<String> variablesUsedInIndex = new LinkedHashSet<>();
        for (VariableIdentifierAst variableIdentifierAst : getIndex().findVariableIdentifierDescendants()) {
            variablesUsedInIndex.add(variableIdentifierAst.getName());
        }
        return variablesUsedInIndex;
    }


    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == table) {
            table = targetChildren;
        } else if (childrenToSwap == index) {
            index = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new GetSubscriptExpressionAst(table.clone(), index.clone());
    }

}
