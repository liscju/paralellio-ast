package pl.edu.agh.ast.operators.bitwise.unary;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Size of given expression.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * sizeof(15);
 * }
 * </pre>
 */
public class SizeOfExpressionAst extends DetailAst{
    private DetailAst argument;

    public SizeOfExpressionAst(DetailAst argument) {
        super(DetailAstUtils.createNonNullsList(argument));
        this.argument = argument;
    }

    public SizeOfExpressionAst(DetailAst argument, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(argument), origStartOffset, origEndOffset);
        this.argument = argument;
    }

    /**
     * Expression to calculate size.
     * @return expression
     */
    public DetailAst getArgument() {
        return argument;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == argument) {
            argument = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new SizeOfExpressionAst(argument.clone());
    }

}
