package pl.edu.agh.ast.operators.booleans;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Left than operator of two expressions.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * 15 < 20;
 * }
 * </pre>
 */
public class LessThanExpresionAst extends DetailAst{
    private DetailAst leftSide;
    private DetailAst rightSide;

    public LessThanExpresionAst(DetailAst leftSide, DetailAst rightSide) {
        super(DetailAstUtils.createNonNullsList(leftSide, rightSide));
        this.leftSide = leftSide;
        this.rightSide = rightSide;
    }

    public LessThanExpresionAst(DetailAst leftSide, DetailAst rightSide, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(leftSide, rightSide), origStartOffset, origEndOffset);
        this.leftSide = leftSide;
        this.rightSide = rightSide;
    }

    /**
     * Left side expression of operator
     * @return expression
     */
    public DetailAst getLeftExpression() {
        return leftSide;
    }

    /**
     * Right side expression of operator
     * @return expression
     */
    public DetailAst getRightExpression() {
        return rightSide;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == leftSide) {
            leftSide = targetChildren;
        } else if (childrenToSwap == rightSide) {
            rightSide = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new LessThanExpresionAst(leftSide.clone(), rightSide.clone());
    }

}
