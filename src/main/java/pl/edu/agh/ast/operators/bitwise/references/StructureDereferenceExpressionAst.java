package pl.edu.agh.ast.operators.bitwise.references;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Dereference value in structure.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * my_structure->a
 * }
 * </pre>
 */
public class StructureDereferenceExpressionAst extends DetailAst{
    private DetailAst variableAst;
    private final String member;

    public StructureDereferenceExpressionAst(DetailAst variableAst, String member) {
        super(DetailAstUtils.createNonNullsList(variableAst));
        this.variableAst = variableAst;
        this.member = member;
    }

    public StructureDereferenceExpressionAst(DetailAst variableAst, String member,
                                             int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(variableAst), origStartOffset, origEndOffset);
        this.variableAst = variableAst;
        this.member = member;
    }

    /**
     * Returns VariableIdentifier(for simple referene like 'a') or StructureReferenceExpressionAst
     */
    /**
     * Return structure from which we have dereference
     * @return structure
     */
    public DetailAst getVariable() {
        return variableAst;
    }

    /**
     * Gets member to dereference from structure
     * @return member
     */
    public String getMember() {
        return member;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == variableAst) {
            variableAst = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new StructureDereferenceExpressionAst(variableAst.clone(), member);
    }

}
