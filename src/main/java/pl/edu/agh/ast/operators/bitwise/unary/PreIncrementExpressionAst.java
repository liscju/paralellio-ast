package pl.edu.agh.ast.operators.bitwise.unary;

import com.google.common.collect.Sets;
import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

import java.util.Set;

/**
 * Pre increment expression value.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * ++value;
 * }
 * </pre>
 */
public class PreIncrementExpressionAst extends DetailAst{
    private DetailAst argument;

    public PreIncrementExpressionAst(DetailAst argument) {
        super(DetailAstUtils.createNonNullsList(argument));
        this.argument = argument;
    }

    public PreIncrementExpressionAst(DetailAst argument, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(argument), origStartOffset, origEndOffset);
        this.argument = argument;
    }

    /**
     * Get expression to calculate pre increment value
     * @return expression
     */
    public DetailAst getArgument() {
        return argument;
    }

    @Override
    public Set<String> getOutputDependencies() {
        return Sets.union(super.getOutputDependencies(),
                super.getInputDependencies());
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == argument) {
            argument = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new PreIncrementExpressionAst(argument.clone());
    }

}
