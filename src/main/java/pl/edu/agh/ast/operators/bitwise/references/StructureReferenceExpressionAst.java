package pl.edu.agh.ast.operators.bitwise.references;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Reference value in structure.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * my_structure.a
 * }
 * </pre>
 */
public class StructureReferenceExpressionAst extends DetailAst{
    private DetailAst variable;
    private final String member;

    public StructureReferenceExpressionAst(DetailAst variable, String member) {
        super(DetailAstUtils.createNonNullsList(variable));
        this.variable = variable;
        this.member = member;
    }

    public StructureReferenceExpressionAst(DetailAst variable, String member, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(variable), origStartOffset, origEndOffset);
        this.variable = variable;
        this.member = member;
    }

    /**
     * Returns VariableIdentifier(for simple referene like 'a') or StructureReferenceExpressionAst
     */
    /**
     * Return structure from which we have reference
     * @return structure
     */
    public DetailAst getVariable() {
        return variable;
    }

    /**
     * Gets member to reference from structure
     * @return member
     */
    public String getMember() {
        return member;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == variable) {
            variable = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new StructureReferenceExpressionAst(variable.clone(), member);
    }

}
