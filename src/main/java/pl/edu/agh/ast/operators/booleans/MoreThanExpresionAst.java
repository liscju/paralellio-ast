package pl.edu.agh.ast.operators.booleans;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Boolean more operator of two expression.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * 20 > 10;
 * }
 * </pre>
 */
public class MoreThanExpresionAst extends DetailAst{
    private DetailAst leftExpression;
    private DetailAst rightExpression;

    public MoreThanExpresionAst(DetailAst leftExpression, DetailAst rightExpression) {
        super(DetailAstUtils.createNonNullsList(leftExpression, rightExpression));
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    public MoreThanExpresionAst(DetailAst leftExpression, DetailAst rightExpression,
                                int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(leftExpression, rightExpression),
              origStartOffset, origEndOffset);
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    /**
     * Get left side expression of operator
     * @return expression
     */
    public DetailAst getLeftExpression() {
        return leftExpression;
    }

    /**
     * Get right side expression of operator
     * @return right side of the expression
     */
    public DetailAst getRightExpression() {
        return rightExpression;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == leftExpression) {
            leftExpression = targetChildren;
        } else if (childrenToSwap == rightExpression) {
            rightExpression = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new MoreThanExpresionAst(leftExpression.clone(), rightExpression.clone());
    }

}
