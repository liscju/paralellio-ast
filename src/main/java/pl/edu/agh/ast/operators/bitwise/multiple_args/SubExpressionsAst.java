package pl.edu.agh.ast.operators.bitwise.multiple_args;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Substracts two expressions.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * 25 - 12;
 * }
 * </pre>
 */
public class SubExpressionsAst extends DetailAst {
    private DetailAst leftSide;
    private DetailAst rightSide;

    public SubExpressionsAst(DetailAst leftSide, DetailAst rightSide) {
        super(DetailAstUtils.createNonNullsList(leftSide, rightSide));
        this.leftSide = leftSide;
        this.rightSide = rightSide;
    }

    public SubExpressionsAst(DetailAst leftSide, DetailAst rightSide, int origStartIndex, int origEndIndex) {
        super(DetailAstUtils.createNonNullsList(leftSide, rightSide), origStartIndex, origEndIndex);
        this.leftSide = leftSide;
        this.rightSide = rightSide;
    }

    /**
     * Get left side of the expression
     * @return left side of the expression
     */
    public DetailAst getLeftExpression() {
        return leftSide;
    }

    /**
     * Get right side of the expression
     * @return right side of the expression
     */
    public DetailAst getRightExpression() {
        return rightSide;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == leftSide) {
            leftSide = targetChildren;
        } else if (childrenToSwap == rightSide) {
            rightSide = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new SubExpressionsAst(leftSide.clone(), rightSide.clone());
    }

}
