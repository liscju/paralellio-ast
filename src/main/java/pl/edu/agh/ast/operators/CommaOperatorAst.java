package pl.edu.agh.ast.operators;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Comma operator
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * 2 > 3, i++, 5 <= 10;
 * }
 * </pre>
 */
public class CommaOperatorAst extends DetailAst{
    private final DetailAst leftExpression;
    private final DetailAst rightExpression;

    public CommaOperatorAst(DetailAst leftExpression, DetailAst rightExpression) {
        super(DetailAstUtils.createNonNullsList(leftExpression, rightExpression));
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    public CommaOperatorAst(DetailAst leftExpression, DetailAst rightExpression,
                            int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(leftExpression, rightExpression),
              origStartOffset, origEndOffset);
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    /**
     * Get left side expression of operator
     * @return expression
     */
    public DetailAst getLeftExpression() {
        return leftExpression;
    }

    /**
     * Get right side expression of operator
     * @return expression
     */
    public DetailAst getRightExpression() {
        return rightExpression;
    }

    @Override
    public DetailAst clone() {
        return new CommaOperatorAst(leftExpression.clone(), rightExpression.clone());
    }

}
