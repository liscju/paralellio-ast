package pl.edu.agh.ast.operators.bitwise.multiple_args;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Add two expressions
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * i + 25;
 * }
 * </pre>
 */
public class AddExpressionsAst extends DetailAst {
    private DetailAst leftSide;
    private DetailAst rightSide;

    public AddExpressionsAst(DetailAst leftSide, DetailAst rightSide) {
        super(DetailAstUtils.createNonNullsList(leftSide, rightSide));
        this.leftSide = leftSide;
        this.rightSide = rightSide;
    }

    public AddExpressionsAst(DetailAst leftSide, DetailAst rightSide, int origStartIndex, int origEndIndex) {
        super(DetailAstUtils.createNonNullsList(leftSide, rightSide),
                origStartIndex, origEndIndex);
        this.leftSide = leftSide;
        this.rightSide = rightSide;
    }

    /**
     * Return left side of the expression
     * @return left side of the expression
     */
    public DetailAst getLeftExpression() {
        return leftSide;
    }

    /**
     * Return right side of the expression
     * @return right side of the expression
     */
    public DetailAst getRightExpression() {
        return rightSide;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == leftSide) {
            leftSide = targetChildren;
        } else if (childrenToSwap == rightSide) {
            rightSide = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new AddExpressionsAst(leftSide.clone(), rightSide.clone());
    }

}
