package pl.edu.agh.ast.jump;

import pl.edu.agh.ast.api.DetailAst;

import java.util.LinkedList;

/**
 * Goto label statement.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * A: w = w + 20;
 * goto A;
 * }
 * </pre>
 */
public class GotoStatementAst extends DetailAst{
    private final String label;

    public GotoStatementAst(String label) {
        super(new LinkedList<>());
        this.label = label;
    }

    public GotoStatementAst(String label, int origStartOffset, int origEndOffset) {
        super(new LinkedList<>(), origStartOffset, origEndOffset);
        this.label = label;
    }

    /**
     * Return label to jump from goto
     * @return label
     */
    public String getLabel() {
        return label;
    }

    @Override
    public DetailAst clone() {
        return new GotoStatementAst(label);
    }

}
