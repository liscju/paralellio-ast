package pl.edu.agh.ast.jump;

import pl.edu.agh.ast.api.DetailAst;

import java.util.LinkedList;

/**
 * Break statement.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * while (true) {
 *     break;
 * }
 * }
 * </pre>
 */
public class BreakStatementAst extends DetailAst{
    public BreakStatementAst() {
        super(new LinkedList<>());
    }

    public BreakStatementAst(int origStartOffset, int origEndOffset) {
        super(new LinkedList<>(), origStartOffset, origEndOffset);
    }

    @Override
    public DetailAst clone() {
        return new BreakStatementAst();
    }

}
