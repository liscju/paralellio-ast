package pl.edu.agh.ast.jump;

import pl.edu.agh.ast.api.DetailAst;

import java.util.LinkedList;

/**
 * Continue statement.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * while (true) {
 *     continue;
 * }
 * }
 * </pre>
 */
public class ContinueStatementAst extends DetailAst{
    public ContinueStatementAst() {
        super(new LinkedList<>());
    }

    public ContinueStatementAst(int origStartOffset, int origEndOffset) {
        super(new LinkedList<>(), origStartOffset, origEndOffset);
    }

    @Override
    public DetailAst clone() {
        return new ContinueStatementAst();
    }

}
