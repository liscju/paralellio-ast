package pl.edu.agh.ast.jump;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

import java.util.LinkedList;

/**
 * Return from function statement.
 *
 * <p> Return have expression to return or not</p>
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * int main() {
 *     return;
 * }
 * }
 * </pre>
 */
public class ReturnStatementAst extends DetailAst{

    private DetailAst returnExpr;

    public ReturnStatementAst() {
        super(new LinkedList<>());
        this.returnExpr = null;
    }

    public ReturnStatementAst(DetailAst returnExpr) {
        super(DetailAstUtils.createNonNullsList(returnExpr));
        this.returnExpr = returnExpr;
    }

    public ReturnStatementAst(int origStartOffset, int origEndOffset) {
        super(new LinkedList<>(), origStartOffset, origEndOffset);
        this.returnExpr = null;
    }

    public ReturnStatementAst(DetailAst returnExpr, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(returnExpr), origStartOffset, origEndOffset);
        this.returnExpr = returnExpr;
    }

    /**
     * Expression to return from method
     * @return expression or null if return doesn't have expression
     */
    public DetailAst getExpression() {
        return returnExpr;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == returnExpr) {
            returnExpr = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        if (returnExpr != null) {
            return new ReturnStatementAst(returnExpr.clone());
        } else
            return new ReturnStatementAst();
    }

}
