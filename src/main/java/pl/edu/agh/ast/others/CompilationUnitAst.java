package pl.edu.agh.ast.others;

import pl.edu.agh.ast.api.CompoundStatementAst;
import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Compilation unit, it consists of 1..n translation units
 */
public class CompilationUnitAst extends CompoundStatementAst {

    private TranslationUnitAst translationUnitAst;

    public CompilationUnitAst(TranslationUnitAst translationUnitAst) {
        super(DetailAstUtils.createNonNullsList(translationUnitAst));
        this.translationUnitAst = translationUnitAst;
    }

    /**
     * Get first translation unit
     * @return translation unit
     */
    public TranslationUnitAst getTranslationUnit() {
        return translationUnitAst;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == translationUnitAst) {
            translationUnitAst = (TranslationUnitAst) targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new CompilationUnitAst((TranslationUnitAst) translationUnitAst.clone());
    }

}
