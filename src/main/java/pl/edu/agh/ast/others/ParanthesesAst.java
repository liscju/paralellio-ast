package pl.edu.agh.ast.others;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Parantheses around expression.
 *
 * <p>
 * Parantheses around expression.
 * </p>
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 *  expression
 *  ^^^
 *  |||
 * (int) j;
 * ^^^^^
 * |||||
 * Parantheses ast
 * }
 * </pre>
 */
public class ParanthesesAst extends DetailAst{
    private DetailAst expression;

    public ParanthesesAst(DetailAst expression) {
        super(DetailAstUtils.createNonNullsList(expression));
        this.expression = expression;
    }

    public ParanthesesAst(DetailAst expression, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(expression),
                origStartOffset, origEndOffset);
        this.expression = expression;
    }

    /**
     * Get expression which parantheses encloses.
     * @return expression
     */
    public DetailAst getExpression() {
        return expression;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == expression) {
            expression = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new ParanthesesAst(expression.clone());
    }

}
