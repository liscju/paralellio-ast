package pl.edu.agh.ast.others;

import pl.edu.agh.ast.api.CompoundStatementAst;
import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;
import pl.edu.agh.ast.statements.StatementListAst;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Function definition.
 *
 * <p>
 * Consists of header(parsed as String) and function body.
 * </p>
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * int main()
 * ^^^^^^^^^^
 * ||||||||||
 * Header as txt
 * {          <----
 *     int i; <---- Body as StatementListAst
 * }          <----
 * }
 * </pre>
 */
public class FunctionDefinitionAst extends CompoundStatementAst {
    private final String name;
    private final String header;
    private final List<String> parameterList;
    private StatementListAst body;

    public FunctionDefinitionAst(String name, List<String> parameterList,
                                 String header, StatementListAst body) {
        super(DetailAstUtils.createNonNullsList(body));
        this.header = header;
        this.name = name;
        this.parameterList = parameterList;
        this.body = body;
    }

    public FunctionDefinitionAst(String name, List<String> parameterList, String header,
                                 StatementListAst body, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(body), origStartOffset, origEndOffset);
        this.header = header;
        this.name = name;
        this.parameterList = parameterList;
        this.body = body;
    }

    /**
     * Get name of the function
     * @return function name
     */
    public String getName() {
        return name;
    }

    /**
     * Get names of parameters of the function
     * @return list of names of parameters
     */
    public List<String> getParameters() {
        return new LinkedList<>(parameterList);
    }

    /**
     * Get header of function definition
     * @return header
     */
    public String getHeader() {
        return header;
    }

    /**
     * Get body of a function
      * @return body
     */
    public StatementListAst getBody() {
        return body;
    }

    @Override
    public Set<String> getOutputDependencies() {
        Set<String> outputDependencies = new LinkedHashSet<>();
        outputDependencies.add(getName());
        outputDependencies.addAll(getParameters());
        return outputDependencies;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == body) {
            body = (StatementListAst) targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new FunctionDefinitionAst(
                name, parameterList, header, (StatementListAst) body.clone());
    }

}
