package pl.edu.agh.ast.others;

import com.google.common.collect.Sets;
import pl.edu.agh.ast.api.DetailAst;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Abstract syntax tree for any variable declaration.
 *
 * <p>
 * For now it contains only text inside, we don't parse
 * variable names, initialization values etc
 * </p>
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * int i, m = 20, j = 30;
 * }
 * </pre>
 */
public class DeclarationAst extends DetailAst{
    private final String declarationText;
    private final LinkedHashMap<String, DetailAst> variables;

    public DeclarationAst(String declarationText, LinkedHashMap<String, DetailAst> variables) {
        super(
                new LinkedList<>(variables.values()
                                          .stream()
                                          .filter(var -> var != null)
                                          .collect(Collectors.toList())));
        this.declarationText = declarationText;
        this.variables = variables;
    }

    public DeclarationAst(String declarationText, LinkedHashMap<String, DetailAst> variables,
                          int origStartOffset, int origEndOffset) {
        super(new LinkedList<>(variables.values()
                .stream()
                .filter(var -> var != null)
                .collect(Collectors.toList())),
              origStartOffset, origEndOffset);
        this.declarationText = declarationText;
        this.variables = variables;
    }

    /**
     * Get variables declared in given declaration
     * @return variables as list of string
     */
    public Set<String> getVariables() {
        return variables.keySet();
    }

    public Map<String, DetailAst> getVariablesInitializers() {
        return variables;
    }

    /**
     * Get declaration text
     * @return declaration text
     */
    public String getDeclarationText() {
        return declarationText;
    }

    @Override
    public Set<String> getInputDependencies() {
        Set<String> declaredVariables = new HashSet<>();
        Set<String> inputDependencies = new LinkedHashSet<>();
        for (Map.Entry<String, DetailAst> variableEntrySet : variables.entrySet()) {
            declaredVariables.add(variableEntrySet.getKey());
            if (variableEntrySet.getValue() != null) {
                inputDependencies.addAll(
                        Sets.difference(variableEntrySet.getValue().getInputDependencies(),
                                declaredVariables));
            }
        }
        return inputDependencies;
    }

    @Override
    public Set<String> getOutputDependencies() {
        return Sets.union(getVariables(), super.getOutputDependencies());
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (variables.values().contains(childrenToSwap)) {
            String keyByValue = getKeyByValue(variables, childrenToSwap);
            variables.put(keyByValue, targetChildren);
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Map.Entry<T, E> entry : map.entrySet()) {

            if (value.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;

    }

    @Override
    public DetailAst clone() {
        LinkedHashMap<String, DetailAst> clonedVariables = new LinkedHashMap<>();
        for (Map.Entry<String, DetailAst> clonedVariableEntry : variables.entrySet()) {
            DetailAst clonedVariableEntryValue;
            if (clonedVariableEntry.getValue() != null)
                clonedVariableEntryValue = clonedVariableEntry.getValue().clone();
            else
                clonedVariableEntryValue = null;
            clonedVariables.put(clonedVariableEntry.getKey(),
                    clonedVariableEntryValue);
        }
        return new DeclarationAst(declarationText, clonedVariables);
    }

}
