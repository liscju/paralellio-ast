package pl.edu.agh.ast.others;

import pl.edu.agh.ast.api.DetailAst;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * Reference to variable in expression.
 *
 * <p>
 * Its parsed just as text with given name.
 * </p>
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * sqrt( my_variable * 30);
 *       ^^^^^^^^^^^
 *       |||||||||||
 *       VariableIdentifierAst
 * }
 * </pre>
 */
public class VariableIdentifierAst extends DetailAst {
    private String name;

    public VariableIdentifierAst(String name) {
        super(new LinkedList<>());
        this.name = name;
    }

    public VariableIdentifierAst(String name, int origStartIndex, int origEndIndex) {
        super(new LinkedList<>(), origStartIndex, origEndIndex);
        this.name = name;
    }

    /**
     * Get name of variable
     * @return variable name
     */
    public String getName() {
        return name;
    }

    /**
     * Set name of variable
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Set<String> getInputDependencies() {
        Set<String> inputDependencies = new LinkedHashSet<>();
        inputDependencies.add(getName());
        return inputDependencies;
    }

    @Override
    public DetailAst clone() {
        return new VariableIdentifierAst(name);
    }

}
