package pl.edu.agh.ast.others;

import pl.edu.agh.ast.api.DetailAst;

import java.util.LinkedList;

/**
 * Abstract syntax tree for a directive.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * #include <stdio.h>
 * }
 * </pre>
 */
public class DirectiveAst extends DetailAst {

    private final String directive;

    public DirectiveAst(String directive) {
        super(new LinkedList<>());
        this.directive = directive;
    }

    /**
     * Gets text of directive.
     *
     * @return text of directive
     */
    public String getDirective() {
        return directive;
    }

}
