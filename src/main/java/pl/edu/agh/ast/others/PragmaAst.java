package pl.edu.agh.ast.others;

import pl.edu.agh.ast.api.DetailAst;

import java.util.LinkedList;

/**
 * Abstract syntax tree for a pragma.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * #pragma omp task final(depth>limit) mergeable <- pragma text
 * traverse(p->left);                            <- children
 * }
 * </pre>
 */
public class PragmaAst extends DetailAst {

    private final String pragmaText;
    private final DetailAst children;

    public PragmaAst(String pragmaText, DetailAst children) {
        super(new LinkedList<DetailAst>() {{
            add(children);
        }});
        this.pragmaText = pragmaText;
        this.children = children;
    }

    @Override
    public boolean isControlStmt() {
        return true;
    }

    /**
     * Gets text of a pragma.
     *
     * @return text of pragma
     */
    public String getPragmaText() {
        return pragmaText;
    }

    /**
     * Gets children of a pragma.
     *
     * @return children of pragma, null if not exist
     */
    public DetailAst getChildren() {
        return children;
    }

}
