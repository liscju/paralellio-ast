package pl.edu.agh.ast.others;

import pl.edu.agh.ast.api.CompoundStatementAst;
import pl.edu.agh.ast.api.DetailAst;

import java.util.LinkedList;
import java.util.List;

/**
 * Translation unit, in general it is one file.
 *
 * <p>
 * In general it consits of many expression of different
 * types for example: function definitions, declarations,
 * etc.
 * </p>
 */
public class TranslationUnitAst extends CompoundStatementAst {

    public TranslationUnitAst(List<DetailAst> childrens) {
        super(childrens);
    }

    /**
     * Get all expression that translation unit contains
     * @return expressions
     */
    public List<DetailAst> getArguments() {
        return getChildrens();
    }


    /**
     * Add child to the translation unit in the given position
     * @param pos position
     * @param child child to add
     */
    public void addChild(int pos, DetailAst child) {
        childrens.add(pos, child);
    }

    @Override
    public DetailAst clone() {
        List<DetailAst> clonedChildrens = new LinkedList<>();
        for (DetailAst children : childrens) {
            clonedChildrens.add(children.clone());
        }
        return new TranslationUnitAst(clonedChildrens);
    }

}
