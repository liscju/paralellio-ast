package pl.edu.agh.ast.literals;

import pl.edu.agh.ast.api.DetailAst;

import java.util.LinkedList;

/**
 * String literal.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * "Hello World\n"
 * }
 * </pre>
 */
public class StringLiteralAst extends DetailAst {
    private String value;

    public StringLiteralAst(String value) {
        super(new LinkedList<>());
        this.value = value;
    }

    public StringLiteralAst(String value, int origStartOffset, int origEndOffset) {
        super(new LinkedList<>(), origStartOffset, origEndOffset);
        this.value = value;
    }

    /**
     * Value of string literal
     * @return string literal
     */
    public String getValue() {
        return value;
    }

    @Override
    public DetailAst clone() {
        return new StringLiteralAst(value);
    }

}
