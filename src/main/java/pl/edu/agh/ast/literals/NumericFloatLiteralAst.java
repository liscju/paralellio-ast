package pl.edu.agh.ast.literals;

import pl.edu.agh.ast.api.DetailAst;

import java.util.LinkedList;

/**
 * Numeric float literal.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * 23.5f
 * }
 * </pre>
 */
public class NumericFloatLiteralAst extends DetailAst {
    private final String value;

    public NumericFloatLiteralAst(String value) {
        super(new LinkedList<>());
        this.value = value;
    }

    public NumericFloatLiteralAst(String value, int origStartIndex, int origEndIndex) {
        super(new LinkedList<>(), origStartIndex, origEndIndex);
        this.value = value;
    }

    /**
     * Return value of Float literal
     * @return float literal
     */
    public String getValue() {
        return value;
    }

    @Override
    public DetailAst clone() {
        return new NumericFloatLiteralAst(value);
    }

}
