package pl.edu.agh.ast.literals;

import pl.edu.agh.ast.api.DetailAst;

import java.util.LinkedList;

/**
 * Integer literal.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * 15
 * }
 * </pre>
 */
public class NumericIntLiteralAst extends DetailAst {
    private final String value;

    public NumericIntLiteralAst(String value) {
        super(new LinkedList<>());
        this.value = value;
    }

    public NumericIntLiteralAst(String value, int startOffset, int endOffset) {
        super(new LinkedList<>(), startOffset, endOffset);
        this.value = value;
    }

    /**
     * Value of literal
     * @return literal
     */
    public String getValue() {
        return value;
    }

    @Override
    public DetailAst clone() {
        return new NumericIntLiteralAst(value);
    }

}
