package pl.edu.agh.ast.literals;


import pl.edu.agh.ast.api.DetailAst;

import java.util.LinkedList;

/**
 * Character literal.
 *
 * <p>Literal for characters</p>
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * 'A'
 * }
 * </pre>
 */
public class CharacterLiteralAst extends DetailAst {
    private final String value;

    public CharacterLiteralAst(String value) {
        super(new LinkedList<>());
        this.value = value;
    }

    public CharacterLiteralAst(String value, int origStartIndex, int origEndIndex) {
        super(new LinkedList<>(), origStartIndex, origEndIndex);
        this.value = value;
    }

    /**
     * Value of literal
     * @return character or empty string if literal is ''
     */
    public String getValue() {
        final String val;
        if (value.equals("\'\'")) {
            val = "";
        }
        else {
            // text is of the form 'x' where x is character
            val = value.substring(1, value.length() - 1);
        }
        return val;
    }

    @Override
    public DetailAst clone() {
        return new CharacterLiteralAst(value);
    }

}
