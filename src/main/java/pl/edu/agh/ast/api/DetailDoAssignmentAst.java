package pl.edu.agh.ast.api;

import java.util.List;
import java.util.Set;

/**
 * Represents assignment of the form:
 * x op= y
 * where op one of +,-,/,|,& etc.
 */
public abstract class DetailDoAssignmentAst extends DetailAssignmentAst {
    public DetailDoAssignmentAst(List<DetailAst> childrens) {
        super(childrens);
    }

    public DetailDoAssignmentAst(List<DetailAst> childrens, int origStartOffset, int origEndOffset) {
        super(childrens, origStartOffset, origEndOffset);
    }

    @Override
    public Set<String> getInputDependencies() {
        Set<String> inputDependencies = super.getInputDependencies();
        inputDependencies.addAll(getOutputDependencies());
        return inputDependencies;
    }
}
