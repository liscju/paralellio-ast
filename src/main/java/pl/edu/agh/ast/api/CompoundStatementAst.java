package pl.edu.agh.ast.api;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents statements containing other statements
 * <p>
 * Compound statement represents all statements that
 * contains other statement like IF contains statement
 * with condition, body, else body.
 * </p>
 */
public class CompoundStatementAst extends DetailAst{
    public CompoundStatementAst(List<DetailAst> childrens) {
        super(childrens);
    }

    public CompoundStatementAst(List<DetailAst> childrens, boolean isLoop) {
        super(childrens, isLoop);
    }

    public CompoundStatementAst(List<DetailAst> childrens, int origStartOffset, int origEndOffset) {
        super(childrens, origStartOffset, origEndOffset);
    }

    public CompoundStatementAst(List<DetailAst> childrens, boolean isLoop, int origStartOffset, int origEndOffset) {
        super(childrens, isLoop, origStartOffset, origEndOffset);
    }

    /**
     * Get real body of compound statements. By real body
     * it means list of statement/statement-list
     * that given compoung statement contains
     * for example for IF:
     * <pre>
     * {@code
     * if (cond)
     *      body
     * else
     *      body_else
     * }
     * </pre>
     * it should return body and body_else(not cond as its
     * part of if!!!!!!!)
     * @return real body of compound statement
     */
    public List<DetailAst> getCompoundStatementBody() {
        return getChildrens();
    }

    @Override
    public boolean isCompoundStatement() {
        return true;
    }

    @Override
    public Set<String> getInputDependencies() {
        return new HashSet<>();
    }

    @Override
    public Set<String> getOutputDependencies() {
        return new HashSet<>();
    }
}
