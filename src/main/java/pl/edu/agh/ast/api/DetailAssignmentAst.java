package pl.edu.agh.ast.api;

import com.google.common.collect.Sets;
import pl.edu.agh.ast.operators.bitwise.references.GetSubscriptExpressionAst;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents assignment of the form:
 * x = y
 */
public abstract class DetailAssignmentAst extends DetailAst{
    public DetailAssignmentAst(List<DetailAst> childrens) {
        super(childrens);
    }

    public DetailAssignmentAst(List<DetailAst> childrens, int origStartOffset, int origEndOffset) {
        super(childrens, origStartOffset, origEndOffset);
    }

    /**
     * Return left side of assignment
     * @return left side of assignment
     */
    public abstract DetailAst getLeftSide();

    /**
     * Return right side of assignment
     * @return right side of assignment
     */
    public abstract DetailAst getRightSide();

    @Override
    public Set<String> getInputDependencies() {
        if (getLeftSide() instanceof GetSubscriptExpressionAst) {
            GetSubscriptExpressionAst subscriptExpressionAst =
                    (GetSubscriptExpressionAst) getLeftSide();
            Set<String> subscriptInputDependencies = subscriptExpressionAst.getInputDependencies();
            String finalTableVariableName = subscriptExpressionAst.getFinalTableVariableName();
            if (finalTableVariableName != null) {
                subscriptInputDependencies.remove(finalTableVariableName);
            }
            Set<String> rightSideInputDependencies = getRightSide().getInputDependencies();
            Sets.SetView<String> depUnion = Sets.union(
                    subscriptInputDependencies
                    , rightSideInputDependencies
            );
            // View is immutable, so create new mutable set
            return new LinkedHashSet<>(depUnion);
        } else
            return getRightSide().getInputDependencies();
    }

    @Override
    public Set<String> getOutputDependencies() {
        if (getLeftSide() instanceof GetSubscriptExpressionAst) {
            GetSubscriptExpressionAst subscriptExpressionAst =
                    (GetSubscriptExpressionAst) getLeftSide();
            Set<String> subscriptOutputDependencies = subscriptExpressionAst.getOutputDependencies();
            String finalTableVariableName = subscriptExpressionAst.getFinalTableVariableName();
            if (finalTableVariableName != null) {
                subscriptOutputDependencies.add(finalTableVariableName);
            }
            Set<String> rightSideOutputDependencies =
                    getRightSide().getOutputDependencies();
            Sets.SetView<String> depUnion = Sets.union(
                    subscriptOutputDependencies
                    , rightSideOutputDependencies
            );
            // View is immutable, so create new mutable set
            return new LinkedHashSet<>(depUnion);
        } else
            return getLeftSide().getInputDependencies();
    }
}
