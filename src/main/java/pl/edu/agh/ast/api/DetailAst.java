package pl.edu.agh.ast.api;

import pl.edu.agh.ast.operators.bitwise.references.GetSubscriptExpressionAst;
import pl.edu.agh.ast.others.VariableIdentifierAst;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Basic class of abstract syntax tree.
 *
 * <p>
 * Created to be subclass by specialised classes,
 * it establish common interface between them.
 * </p>
 */
public class DetailAst {
    protected final List<DetailAst> childrens;
    private boolean isLoop;
    private int origStartOffset = -1;
    private int origEndOffset = -1;

    public DetailAst(List<DetailAst> childrens) {
        this.childrens = childrens;
        isLoop = false;
    }


    public DetailAst(List<DetailAst> childrens,
                     int origStartOffset,
                     int origEndOffset) {
        this(childrens);
        this.origStartOffset = origStartOffset;
        this.origEndOffset = origEndOffset;
    }

    public DetailAst(List<DetailAst> childrens,
                     boolean isLoop) {
        this(childrens);
        this.isLoop = isLoop;
    }

    public DetailAst(List<DetailAst> childrens,
                     boolean isLoop,
                     int origStartOffset,
                     int origEndOffset) {
        this(childrens, true);
        this.origStartOffset = origStartOffset;
        this.origEndOffset = origEndOffset;
    }

    /**
     * Perform deep cloning on the ast and its children
     * @return deep clone of the object
     */
    public DetailAst clone() {
        throw new UnsupportedOperationException(
                "You must implement it in your subclass"
        );
    }

    /**
     * Get childrens of a given ast.
     * @return childrens
     */
    public List<DetailAst> getChildrens() {
        return new LinkedList<DetailAst>(childrens);
    }

    /**
     * Get count of child.
     * @return count of child
     */
    public int getChildrenCount() {
        return childrens.size();
    }

    /**
     * Helper method to get first child.
     * @return first child
     */
    public DetailAst getFirstChild() {
        return childrens.get(0);
    }

    /**
     * Exchange children of ast given by childrenToSwap to targetChildren.
     * It assumes that childrenToSwap exist in child list, in other case
     * it throw IllegalStateException. Classes extending DetailAst should
     * override exchangeChildren to update their reference.
     * @param childrenToSwap children to swap
     * @param targetChildren target children
     */
    public void exchangeChildren(DetailAst childrenToSwap,
                                 DetailAst targetChildren) {
        if (childrenToSwap == null)
            throw new IllegalStateException("children to swap cannot be null");
        int indexOfChildren = childrens.indexOf(childrenToSwap);
        if (indexOfChildren >= 0) {
            childrens.set(indexOfChildren, targetChildren);
            return;
        }
        throw new IllegalStateException(
                "childrenToSwap must exist to swap"
        );
    }

    /**
     * Tells if ast is a loop
     * @return true if ast is a loop, false otherwise
     */
    public boolean isLoop() {
        return isLoop;
    }

    /**
     * Is Ast a control statement?
     * @return true if its a control statement
     */
    public boolean isControlStmt() {
        return false;
    }

    /**
     * Is ast a compound statement?
     * @return true if its a compound statement
     */
    public boolean isCompoundStatement() {
        return false;
    }

    /**
     * Calculate input dependencies of the instruction
     * @return input dependencies as set of variables
     */
    public Set<String> getInputDependencies() {
        Set<String> inputDependencies = new LinkedHashSet<>();
        for (DetailAst detailAst : getChildrens()) {
            inputDependencies.addAll(detailAst.getInputDependencies());
        }
        return inputDependencies;
    }

    /**
     * Calculate output dependencies of the instruction
     * @return output dependencies as set of variables
     */
    public Set<String> getOutputDependencies() {
        Set<String> outputDependencies = new LinkedHashSet<>();
        for (DetailAst detailAst : getChildrens()) {
            outputDependencies.addAll(detailAst.getOutputDependencies());
        }
        return outputDependencies;
    }

    /**
     * Find descendants with given type as class
     * @param class_
     * @return descendants with given type
     */
    public <T> List<T> findDescendantsByType(Class<T> class_) {
        final List<T> descendansByType = new LinkedList<>();
        if (class_.isInstance(this)) {
            descendansByType.add((T) this);
        }
        for (DetailAst detailAst : getChildrens()) {
            descendansByType.addAll(detailAst.findDescendantsByType(class_));
        }
        return descendansByType;
    }

    /**
     * Find all descendants that are loop instructions.
     * <p>
     * If current node is a loop it is added to list of descendants.
     *
     * @return list of asts being loops
     */
    public List<DetailAst> findLoopDescendants() {
        final List<DetailAst> loopDescendants = new LinkedList<>();
        if (isLoop()) {
            loopDescendants.add(this);
        }
        for (DetailAst childAst : getChildrens()) {
            loopDescendants.addAll(childAst.findLoopDescendants());
        }
        return loopDescendants;
    }

    /**
     * Find subscript expression in descendants of given node
     * @return all subscript expression that are descendants of
     *         current ast
     */
    public List<GetSubscriptExpressionAst> findSubscriptExprDescendants() {
        List<GetSubscriptExpressionAst> subscriptExprs = new LinkedList<>();
        if (this instanceof GetSubscriptExpressionAst) {
            subscriptExprs.add((GetSubscriptExpressionAst) this);
        }
        for (DetailAst children : getChildrens()) {
            subscriptExprs.addAll(children.findSubscriptExprDescendants());
        }
        return subscriptExprs;
    }

    /**
     * Find variable identifier in descendants of given node
     * @return all variable identifier that are descendants of
     *         current ast
     */
    public List<VariableIdentifierAst> findVariableIdentifierDescendants() {
        List<VariableIdentifierAst> variableIds = new LinkedList<>();
        if (this instanceof VariableIdentifierAst) {
            variableIds.add((VariableIdentifierAst) this);
        }
        for (DetailAst childAst : getChildrens()) {
            List<VariableIdentifierAst> varIdDescs =
                    childAst.findVariableIdentifierDescendants();
            variableIds.addAll(varIdDescs);
        }
        return variableIds;
    }

    /**
     * Find subscript expression in descendants of given node for given table
     * @param table table to find
     * @return list of subscript expression
     */
    public List<GetSubscriptExpressionAst> findSubscriptExprDescendants(String table) {
        List<GetSubscriptExpressionAst> foundSubscriptExprs = new LinkedList<>();
        List<GetSubscriptExpressionAst> subscriptExprs =
                findSubscriptExprDescendants();
        for (GetSubscriptExpressionAst subscriptExpr : subscriptExprs) {
            if (!(subscriptExpr.getTable() instanceof VariableIdentifierAst))
                continue;
            VariableIdentifierAst subscriptExprDescendantTable =
                    (VariableIdentifierAst) subscriptExpr.getTable();
            if (table.equals(subscriptExprDescendantTable.getName()))
                foundSubscriptExprs.add(subscriptExpr);
        }
        return foundSubscriptExprs;
    }

    /**
     * Get original start offset in source code.
     *
     * Start offset is an offset of a first character of a token in
     * the source code. Offset is a number in range (0, size_of_file-1).
     *
     * @return start offset in original source code,
     *         -1 if ast not exist in original source code
     */
    public int getOrigStartOffset() {
        return origStartOffset;
    }

    /**
     * Get original end offset in source code.
     *
     * End offset is an offset of a last character of a token in
     * the source code. Offset is a number in range (0, size_of_file-1).
     *
     * @return end offset in original source code,
     *         -1 if ast not exist in original source code
     */
    public int getOrigEndOffset() {
        return origEndOffset;
    }

    /**
     * Found parent of given child in given root of the ast
     * @param rootAst root of the ast
     * @param childToFoundParent child whose parent we find
     * @return parent of given child or null if not exist
     */
    public static DetailAst getParent(DetailAst rootAst,
                                      DetailAst childToFoundParent) {
        DetailAst traverseAst = rootAst;
        for (DetailAst childAst : traverseAst.getChildrens()) {
            if (childAst == childToFoundParent)
                return rootAst;
            DetailAst parentFoundInChildAst =
                    DetailAst.getParent(childAst, childToFoundParent);
            if (parentFoundInChildAst != null)
                return parentFoundInChildAst;
        }
        return null;
    }

    /**
     * Find loop enclosing given element.
     * @return null if there is no outer loop,
     *         ast when found
     */
    public static DetailAst findOuterLoop(DetailAst rootAst,
                                          DetailAst childToFoundOuterLoop) {
        DetailAst outerLoop = null;
        DetailAst astIterator =
                DetailAst.getParent(rootAst, childToFoundOuterLoop);
        while (astIterator != null) {
            if (astIterator.isLoop()) {
                outerLoop = astIterator;
                break;
            }
            astIterator = DetailAst.getParent(rootAst, astIterator);
        }
        return outerLoop;
    }

    /**
     * Exchange {@code oldAst} in the ast with {@code newAst}.
     * @param rootAst root of the ast
     * @param oldAst ast to exchange
     * @param newAst new ast value
     * @throws IllegalArgumentException when {@code oldAst} is not found in tree
     *                                  with root {@code rootAst}
     */
    public static void exchange(DetailAst rootAst,
                                DetailAst oldAst,
                                DetailAst newAst) {
        DetailAst parent = DetailAst.getParent(rootAst, oldAst);
        if (parent == null || !parent.getChildrens().contains(oldAst))
            throw new IllegalArgumentException(
                    "Ast to replace not found in root ast"
            );
        parent.exchangeChildren(oldAst, newAst);
    }
}
