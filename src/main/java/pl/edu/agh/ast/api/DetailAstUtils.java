package pl.edu.agh.ast.api;

import java.util.LinkedList;
import java.util.List;

/**
 * Contains miscelannous utils to make creating ast easy
 */
public class DetailAstUtils {

    /**
     * Creates non null list from given DetailAst
     * @param detailAsts list of asts(can contain null)
     * @return non null list of given arguments
     */
    public static List<DetailAst> createNonNullsList(DetailAst... detailAsts) {
        List<DetailAst> nonNullsList = new LinkedList<>();
        for (DetailAst detailAst : detailAsts) {
            if (detailAst != null) {
                nonNullsList.add(detailAst);
            }
        }
        return nonNullsList;
    }
}
