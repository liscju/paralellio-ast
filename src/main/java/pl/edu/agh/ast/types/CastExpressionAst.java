package pl.edu.agh.ast.types;

import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

/**
 * Cast expression.
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 * type
 * |||||
 * (int) 23.4f
 *       ^^^^^
 *       |||||
 *       expression
 * }
 * </pre>
 */
public class CastExpressionAst extends DetailAst{
    private final String castType;
    private DetailAst expression;

    public CastExpressionAst(String castType, DetailAst expression) {
        super(DetailAstUtils.createNonNullsList(expression));
        this.castType = castType;
        this.expression = expression;
    }

    public CastExpressionAst(String castType, DetailAst expression, int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(expression), origStartOffset, origEndOffset);
        this.castType = castType;
        this.expression = expression;
    }

    /**
     * Get type of cast
     * @return type
     */
    public String getType() {
        return castType;
    }

    /**
     * Get expression of cast
     * @return cast
     */
    public DetailAst getExpression() {
        return expression;
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == expression) {
            expression = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        return new CastExpressionAst(castType, expression.clone());
    }

}
