package pl.edu.agh.ast.loops;

import com.google.common.collect.Sets;
import pl.edu.agh.ast.api.CompoundStatementAst;
import pl.edu.agh.ast.api.DetailAst;
import pl.edu.agh.ast.api.DetailAstUtils;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * For loop
 *
 * <p> For example: </p>
 * <pre>
 * {@code
 *     initializer   expression     incrementer
 *     ||||||||||    |||||||||||    ||||||||||
 * for (int i = 0;   i < 12356 ;    i = i + 56)
 * {                   <------------
 *      j = j + 5;     <------------ action
 * }                   <------------
 * }
 * </pre>
 */
public class ForStatementAst extends CompoundStatementAst {
    private DetailAst initializer;
    private DetailAst expression;
    private DetailAst incrementer;
    private DetailAst action;

    public ForStatementAst(DetailAst initializer, DetailAst expression,
                            DetailAst incrementer, DetailAst action) {
        super(DetailAstUtils.createNonNullsList(initializer, expression, incrementer, action),
                true);
        this.initializer = initializer;
        this.expression = expression;
        this.incrementer = incrementer;
        this.action = action;
    }

    public ForStatementAst(DetailAst initializer, DetailAst expression,
                           DetailAst incrementer, DetailAst action,
                           int origStartOffset, int origEndOffset) {
        super(DetailAstUtils.createNonNullsList(initializer, expression, incrementer, action),
                true, origStartOffset, origEndOffset);
        this.initializer = initializer;
        this.expression = expression;
        this.incrementer = incrementer;
        this.action = action;
    }

    /**
     * Get initializer of a loop
     * @return initializer
     */
    public DetailAst getInitializer() {
        return initializer;
    }

    /**
     * Get expression of a loop
     * @return expression
     */
    public DetailAst getExpression() {
        return expression;
    }

    /**
     * Get incrementer of a loop
     * @return incrementer
     */
    public DetailAst getIncrementer() {
        return incrementer;
    }

    /**
     * Get action of a loop
     * @return action
     */
    public DetailAst getAction() {
        return action;
    }

    @Override
    public List<DetailAst> getCompoundStatementBody() {
        return DetailAstUtils.createNonNullsList(action);
    }

    @Override
    public boolean isControlStmt() {
        return true;
    }

    @Override
    public Set<String> getInputDependencies() {
        Set<String> initializerInputDependencies = new LinkedHashSet<>();
        if (getInitializer() != null) {
            initializerInputDependencies.addAll(getInitializer().getInputDependencies());
        }
        Set<String> expressionInputDependencies = new LinkedHashSet<>();
        if (getExpression() != null) {
            expressionInputDependencies.addAll(getExpression().getInputDependencies());
        }
        Set<String> incrementerInputDependencies = new LinkedHashSet<>();
        if (getIncrementer() != null) {
            incrementerInputDependencies.addAll(getIncrementer().getInputDependencies());
        }
        return Sets.union(initializerInputDependencies,
                          Sets.union(expressionInputDependencies, incrementerInputDependencies));
    }

    @Override
    public Set<String> getOutputDependencies() {
        Set<String> initializerOutputDependencies = new LinkedHashSet<>();
        if (getInitializer() != null) {
            initializerOutputDependencies.addAll(getInitializer().getOutputDependencies());
        }
        Set<String> expressionOutputDependencies = new LinkedHashSet<>();
        if (getExpression() != null) {
            expressionOutputDependencies.addAll(getExpression().getOutputDependencies());
        }
        Set<String> incrementerOutputDependencies = new LinkedHashSet<>();
        if (getIncrementer() != null) {
            incrementerOutputDependencies.addAll(getIncrementer().getOutputDependencies());
        }
        return Sets.union(initializerOutputDependencies,
                Sets.union(expressionOutputDependencies, incrementerOutputDependencies));
    }

    @Override
    public void exchangeChildren(DetailAst childrenToSwap, DetailAst targetChildren) {
        if (childrenToSwap == initializer) {
            initializer = targetChildren;
        } else if (childrenToSwap == expression) {
            expression = targetChildren;
        } else if (childrenToSwap == incrementer) {
            incrementer = targetChildren;
        } else if (childrenToSwap == action) {
            action = targetChildren;
        }
        super.exchangeChildren(childrenToSwap, targetChildren);
    }

    @Override
    public DetailAst clone() {
        DetailAst clonedInitializer = null;
        if (initializer != null) {
            clonedInitializer = initializer.clone();
        }

        DetailAst clonedExpression = null;
        if (expression != null) {
            clonedExpression = expression.clone();
        }

        DetailAst clonedIncrementer = null;
        if (incrementer != null) {
            clonedIncrementer = incrementer.clone();
        }


        DetailAst clonedAction = null;
        if (action != null) {
            clonedAction = action.clone();
        }

        return new ForStatementAst(
                clonedInitializer, clonedExpression, clonedIncrementer, clonedAction);
    }

}
